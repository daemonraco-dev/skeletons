package main

import (
	"context"

	"go.uber.org/fx"
)

// Main code.
func run(ctx context.Context) error {
	// TODO add running logic here.

	return nil
}

// Internals.
func main() {
	ctx := context.Background()

	app := fx.New(
		fx.Invoke(fxRunner),
		fx.NopLogger,
	)

	if app.Err() != nil {
		panic(app.Err().Error)
	} else {
		if err := app.Start(ctx); err != nil {
			panic(err)
		}
	}
}

func fxRunner(lifecycle fx.Lifecycle) {
	lifecycle.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			return run(ctx)
		},
	})
}
