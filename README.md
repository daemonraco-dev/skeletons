# How to install
```
sudo apt-get install cookiecutter
```

## Using [VSCoder](https://gitlab.com/daemonraco-dev/vscoder)
```
dev-helper apt-install cookiecutter
```

# How to use
```
cookiecutter https://gitlab.com/daemonraco-dev/skeletons --directory=<template-name>
```

# Templates
* `go-basic-app`: Simple command-line application.
    * Libraries:
        * [Uber Fx](https://uber-go.github.io/fx/)
* `go-htmx-web-app`: Web application using HTMX.
    * Libraries:
        * [HTMX](https://htmx.org/)
        * [Uber Fx](https://uber-go.github.io/fx/)
        * [Chi](https://go-chi.io/)

