package configs

// ___
// Public types.
type Configs struct {
	Cache ConfigCache `mapstructure:"cache"`
}

type ConfigCache struct {
	MaxAge int `mapstructure:"max-age"`
}
