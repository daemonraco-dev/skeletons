package configs

import (
	"fmt"
	"os"

	"github.com/spf13/viper"
	"go.uber.org/fx"
)

// ___
// Public constants and variables.
var ConfigsModule = fx.Options(
	fx.Provide(func() (*Configs, error) {
		configs := &Configs{}

		err := configs.load()
		if err != nil {
			return nil, err
		}

		err = configs.expand()
		if err != nil {
			return nil, err
		}

		return configs, nil
	}),
)

// ___
// Private functions.
func (c *Configs) expand() error {
	return nil
}

func (c *Configs) load() error {
	reader := viper.New()

	configsMainPath := os.Getenv("{{cookiecutter.project_constant}}_CONFIGS_MAIN")
	if configsMainPath == "" {
		return fmt.Errorf("environment variable '{{cookiecutter.project_constant}}_CONFIGS_MAIN' not configured")
	}

	if _, err := os.Stat(configsMainPath); err != nil {
		return fmt.Errorf("path '%s' does not exists", configsMainPath)
	}

	reader.SetConfigFile(configsMainPath)
	if err := reader.ReadInConfig(); err != nil {
		return fmt.Errorf("unable to read main configuration: %s", err.Error())
	}

	configsLocalPath := os.Getenv("{{cookiecutter.project_constant}}_CONFIGS_LOCAL")
	if configsLocalPath != "" {
		if _, err := os.Stat(configsLocalPath); err != nil {
			return fmt.Errorf("path '%s' does not exists", configsLocalPath)
		}

		reader.SetConfigFile(configsLocalPath)
		if err := reader.MergeInConfig(); err != nil {
			return fmt.Errorf("unable to read local configuration: %s", err.Error())
		}
	}

	if err := reader.Unmarshal(c); err != nil {
		return fmt.Errorf("unable to decode configuration: %s", err.Error())
	}

	return nil
}