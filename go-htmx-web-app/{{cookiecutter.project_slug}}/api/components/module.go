package components

import (
	"go.uber.org/fx"

	"{{cookiecutter.repository}}/api/components/examples"
	"{{cookiecutter.repository}}/api/components/index"
)

var Module = fx.Module(
	"components",

	examples.Module,
	index.Module,
)