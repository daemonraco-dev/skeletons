package examples

import "go.uber.org/fx"

// ___
// Private constants and variables.
var Module = fx.Module(
	"components/examples",

	HandlerModule,
	ServiceModule,
)
