package examples

import (
	"net/http"

	"github.com/a-h/templ"
	"go.uber.org/fx"

	"{{cookiecutter.repository}}/api/utils"
	"{{cookiecutter.repository}}/templates/errors"
	"{{cookiecutter.repository}}/templates/layouts"
	pexamples "{{cookiecutter.repository}}/templates/pages/examples"
)

// ___
// Public types.
type IHandler interface {
	List(w http.ResponseWriter, r *http.Request)
}
type Handler struct {
	service IService
}

// ___
// Public constants and variables.
var HandlerModule = fx.Options(
	fx.Provide(func(service IService) IHandler {
		return &Handler{
			service: service,
		}
	}),
)

// ___
// Public functions.
func (h Handler) List(w http.ResponseWriter, r *http.Request) {
	var content templ.Component

	examples, err := h.service.GetAll(r.Context(), utils.ReadPageParams(r))
	if err != nil {
		content = errors.InternalServerError(err.Error())
	} else {
		content = pexamples.ExamplesList(examples)
	}

	layout := layouts.Default(content, &layouts.DefaultOptions{Title: "Examples"})
	layout.Render(r.Context(), w)
}