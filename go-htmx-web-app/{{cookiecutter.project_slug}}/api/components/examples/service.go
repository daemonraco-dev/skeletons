package examples

import (
	"context"

	"go.uber.org/fx"

	"{{cookiecutter.repository}}/api/types"
	"{{cookiecutter.repository}}/api/utils"
)

// ___
// Public types.
type IService interface {
	GetAll(ctx context.Context, pageParams utils.IPageParams) ([]*types.ExampleItem, error)
}
type Service struct{}

// ___
// Public constants and variables.
var ServiceModule = fx.Options(
	fx.Provide(func() IService {
		return &Service{}
	}),
)

// ___
// Public functions.
func (r Service) GetAll(ctx context.Context, pageParams utils.IPageParams) ([]*types.ExampleItem, error) {
	return []*types.ExampleItem{
		{
			Label: "Some Item",
			Value: "Some Value",
		},
	}, nil
}