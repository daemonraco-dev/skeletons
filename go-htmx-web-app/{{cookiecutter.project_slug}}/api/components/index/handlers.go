package index

import (
	"net/http"

	"go.uber.org/fx"

	"{{cookiecutter.repository}}/templates/layouts"
	"{{cookiecutter.repository}}/templates/pages"
)

type IHandlers interface {
	Default(w http.ResponseWriter, r *http.Request)
}
type Handlers struct{}

var HandlersModule = fx.Options(
	fx.Provide(func() IHandlers {
		return &Handlers{}
	}),
)

func (h Handlers) Default(w http.ResponseWriter, r *http.Request) {
	content := pages.Index()

	layout := layouts.Default(content, &layouts.DefaultOptions{Title: "Home"})
	layout.Render(r.Context(), w)
}