package router

import "go.uber.org/fx"

// ___
// Public constants and variables.
var Module = fx.Module(
	"router",

	fx.Provide(New),

	fx.Invoke(RegisterMiddlewares),
	fx.Invoke(RegisterRoutes),
	fx.Invoke(RegisterErrorRoutes),
	fx.Invoke(RegisterStaticRoutes),
)
