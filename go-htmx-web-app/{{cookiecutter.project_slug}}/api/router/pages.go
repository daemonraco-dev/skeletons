package router

import (
	"github.com/go-chi/chi/v5"

	"{{cookiecutter.repository}}/api/components/examples"
	"{{cookiecutter.repository}}/api/components/index"
)

func RegisterRoutes(
	router *chi.Mux,

	examplesHandler examples.IHandler,
	indexHandler index.IHandlers,
) {
	//
	// Home.
	router.Get("/", indexHandler.Default)
	//
	// Pages.
	router.Route("/examples", func(router chi.Router) {
		router.Get("/", examplesHandler.List)
	})
}