package router

import (
	"fmt"
	"net/http"
	"os"

	"github.com/go-chi/chi/v5"
)

func RegisterStaticRoutes(router *chi.Mux) error {
	assetsDir := os.Getenv("{{cookiecutter.project_constant}}_ASSETS_DIR")
	if assetsDir == "" {
		return fmt.Errorf("environment variable '{{cookiecutter.project_constant}}_ASSETS_DIR' not configured")
	}

	if _, err := os.Stat(assetsDir); err != nil {
		return fmt.Errorf("directory '%s' does not exists", assetsDir)
	}

	fs := http.FileServer(http.Dir(assetsDir))
	router.Handle("/assets/*", http.StripPrefix("/assets/", fs))

	router.Get("/favicon.ico", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Cache-Control", "max-age=259200")
		http.ServeFile(w, r, assetsDir+"/images/favicon.ico")
	})

	return nil
}