package router

import (
	"net/http"

	"github.com/go-chi/chi/v5"

	"{{cookiecutter.repository}}/templates/errors"
	"{{cookiecutter.repository}}/templates/layouts"
)

func RegisterErrorRoutes(router *chi.Mux) {
	router.NotFound(func(w http.ResponseWriter, r *http.Request) {
		content := errors.NotFound(r.RequestURI)

		w.WriteHeader(http.StatusNotFound)
		layout := layouts.Default(content, &layouts.DefaultOptions{PageTitle: "404: Not Found"})
		layout.Render(r.Context(), w)
	})
}