package router

import (
	"context"
	"net/http"
	"runtime/debug"
	"strings"

	"github.com/a-h/templ"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"

	"{{cookiecutter.repository}}/api/constants"
	"{{cookiecutter.repository}}/api/types"
	"{{cookiecutter.repository}}/templates/errors"
	"{{cookiecutter.repository}}/templates/layouts"
)

func RegisterMiddlewares(router *chi.Mux) {
	router.Use(middleware.Logger)
	router.Use(cacheAssets)
	router.Use(htmxSetContext())

	router.Use(recovery)
	// router.Use(middleware.Recoverer)
}

func cacheAssets(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.HasPrefix(r.RequestURI, "/assets") {
			w.Header().Set("Cache-Control", "max-age=259200")
		}

		next.ServeHTTP(w, r)
	})
}

func htmxSetContext() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			values := types.HtmxContext{
				RequestURI: r.RequestURI,
				CurrentUrl: types.HtmxContextUrl{
					Full:        r.Header.Get("Hx-Current-Url"),
					Path:        r.URL.Path,
					QueryParams: r.URL.Query(),
				},
				IsHTMX:   r.Header.Get("Hx-Request") == "true",
				Managers: types.HtmxContextManagers{},
				Target:   r.Header.Get("Hx-Target"),
			}

			ctx := context.WithValue(r.Context(), constants.CtxHtmxContext, values)
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}

func recovery(next http.Handler) http.Handler {
	//
	// Parts of this middleware are based on these links:
	//  * https://github.com/go-chi/chi/blob/master/middleware/recoverer.go
	//  * https://medium.com/@masnun/panic-recovery-middleware-for-go-http-handlers-51147c941f9

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			err := recover()
			if err != nil {
				if err == http.ErrAbortHandler {
					//
					// We don't recover http.ErrAbortHandler so the response to
					// the client is aborted, this should not be logged.
					panic(err)
				}

				logEntry := middleware.GetLogEntry(r)
				if logEntry != nil {
					logEntry.Panic(err, debug.Stack())
				} else {
					middleware.PrintPrettyStack(err)
				}

				var content templ.Component
				if _, ok := err.(error); ok {
					content = errors.InternalServerError(err.(error).Error())
				} else {
					content = errors.InternalServerError("There was an internal server error")
				}

				if r.Header.Get("Connection") != "Upgrade" {
					w.WriteHeader(http.StatusInternalServerError)
				}
				layout := layouts.Default(content, &layouts.DefaultOptions{PageTitle: "500: Internal Server Error"})
				layout.Render(r.Context(), w)
			}
		}()

		next.ServeHTTP(w, r)
	})
}