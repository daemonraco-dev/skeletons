package router

import (
	"github.com/go-chi/chi/v5"
)

func New() *chi.Mux {
	return chi.NewRouter()
}
