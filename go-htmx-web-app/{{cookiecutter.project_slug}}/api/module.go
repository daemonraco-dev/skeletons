package api

import (
	"go.uber.org/fx"

	"{{cookiecutter.repository}}/api/components"
	"{{cookiecutter.repository}}/api/configs"
	"{{cookiecutter.repository}}/api/router"
	"{{cookiecutter.repository}}/api/utils"
)

var Module = fx.Module(
	"api",

	components.Module,
	configs.Module,
	router.Module,
	utils.Module,
)
