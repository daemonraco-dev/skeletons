package types

// ___
// Public types.
type IPageHandler interface {
	GetIsAction() bool
	GetIcon() string
	GetIsExternal() bool
	GetLabel() string
	GetURL() string
	GetWeight() int
}

type PageHandler struct {
	IsAction   bool
	Icon       string
	IsExternal bool
	Label      string
	URL        string
	Weight     int
}

// ___
// Public functions.
func (ph PageHandler) GetIsAction() bool {
	return ph.IsAction
}

func (ph PageHandler) GetIcon() string {
	return ph.Icon
}

func (ph PageHandler) GetIsExternal() bool {
	return ph.IsExternal
}

func (ph PageHandler) GetLabel() string {
	return ph.Label
}

func (ph PageHandler) GetURL() string {
	return ph.URL
}

func (ph PageHandler) GetWeight() int {
	if ph.Weight < 1 {
		return 50
	}
	return ph.Weight
}