package types

// ___
// Public types.
type ILink interface {
	GetIcon() string
	GetIsAction() bool
	GetLabel() string
	GetTitle() string
	GetURL() string
}

type Link struct {
	Icon     string
	IsAction bool
	Label    string
	Title    string
	URL      string
}

// ___
// Public functions.
func (l Link) GetIcon() string {
	return l.Icon
}

func (l Link) GetIsAction() bool {
	return l.IsAction
}

func (l Link) GetLabel() string {
	return l.Label
}

func (l Link) GetTitle() string {
	return l.Title
}

func (l Link) GetURL() string {
	return l.URL
}