package utils

import (
	"encoding/json"
	"math"
	"net/http"
	"strconv"
)

// ___
// Public types.
type IPageParams interface {
	GetPage() int32
	GetPageCount() int32
	GetRowCount() int64
	GetSize() int32
	GetOffset() int32
	SetRowCount(count int64)

	GetNext() int32
	GetPrevious() int32
	GetSteps() []int32
}

type PageParams struct {
	Page      int32 `json:"page"`
	PageCount int32 `json:"pageCount"`
	RowCount  int64 `json:"rowCount"`
	Size      int32 `json:"size"`
	Offset    int32 `json:"offset"`

	Next     int32   `json:"next"`
	Previous int32   `json:"previous"`
	Steps    []int32 `json:"steps"`
}

// ___
// Public functions.
func ReadPageParams(r *http.Request) IPageParams {
	pageStr := r.URL.Query().Get("page")
	pageSizeStr := r.URL.Query().Get("pageSize")

	if pageStr == "" {
		pageStr = "1"
	}
	if pageSizeStr == "" {
		pageSizeStr = "30"
	}

	page, err := strconv.ParseInt(pageStr, 10, 32)
	if err != nil || page < 1 {
		page = 1
	}
	pageSize, err := strconv.ParseInt(pageSizeStr, 10, 32)
	if err != nil || pageSize < 1 {
		pageSize = 30
	} else if pageSize > 200 {
		pageSize = 200
	}

	offset := (page - 1) * pageSize

	return &PageParams{
		Page:   int32(page),
		Size:   int32(pageSize),
		Offset: int32(offset),

		RowCount:  -1,
		PageCount: -1,
	}
}

func Redirect(w http.ResponseWriter, url string) {
	values := map[string]string{
		"path":   url,
		"target": "#main-content",
	}

	location, _ := json.Marshal(values)
	w.Header().Set("Hx-Location", string(location))

	w.WriteHeader(http.StatusOK)
}

func (pp PageParams) GetPage() int32 {
	return pp.Page
}

func (pp PageParams) GetPageCount() int32 {
	return pp.PageCount
}

func (pp PageParams) GetRowCount() int64 {
	return pp.RowCount
}

func (pp PageParams) GetSize() int32 {
	return pp.Size
}

func (pp PageParams) GetOffset() int32 {
	return pp.Offset
}

func (pp *PageParams) SetRowCount(count int64) {
	pp.RowCount = count
	pp.PageCount = int32(math.Ceil(float64(pp.RowCount) / float64(pp.Size)))

	if pp.PageCount < 1 {
		pp.PageCount = 1
	}

	pp.Previous = pp.Page - 1
	pp.Next = pp.Page + 1
	if pp.Previous < 1 {
		pp.Previous = 1
	}
	if pp.Next > pp.PageCount {
		pp.Next = pp.PageCount
	}

	for i := pp.Page - 5; i <= pp.Page+5; i++ {
		if i >= 1 && i <= pp.PageCount {
			pp.Steps = append(pp.Steps, i)
		}
	}
}

func (pp PageParams) GetNext() int32 {
	return pp.Next
}

func (pp PageParams) GetPrevious() int32 {
	return pp.Previous
}

func (pp PageParams) GetSteps() []int32 {
	return pp.Steps
}
