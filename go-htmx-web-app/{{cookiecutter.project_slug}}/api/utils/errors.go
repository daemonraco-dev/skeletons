package utils

import (
	"fmt"
	"net/http"

	"github.com/a-h/templ"

	"{{cookiecutter.repository}}/templates/errors"
	"{{cookiecutter.repository}}/templates/tools"
)

// ___
// Public functions.
func ErrorOrAlert(isGet bool, status int, message string, targetID string) templ.Component {
	var content templ.Component

	switch status {
	case http.StatusOK:
		if !isGet {
			content = tools.OOBAlertWithMessage(message, targetID, "success", true)
		}
	case http.StatusNotFound:
		if isGet {
			content = errors.NotFound(message)
		} else {
			message = fmt.Sprintf("Page '%s' was not found.", message)
			content = tools.OOBAlertWithMessage(message, targetID, "warning", true)
		}
	case http.StatusBadRequest:
		if isGet {
			content = errors.BadRequest(message)
		} else {
			content = tools.OOBAlertWithMessage(message, targetID, "warning", true)
		}
	default:
		if isGet {
			content = errors.InternalServerError(message)
		} else {
			content = tools.OOBAlertWithMessage(message, targetID, "danger", true)
		}
	}

	return content
}
