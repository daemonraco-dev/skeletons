/**
 * Generators:
 * This project depends on multiple generators, if these are not installed
 * globally, these commands should be run first.
 * - go install github.com/a-h/templ/cmd/templ@latest
 */

package main

import (
	"context"
	"log"
	"net/http"
	"os"

	"github.com/go-chi/chi/v5"
	"go.uber.org/fx"

	"{{cookiecutter.repository}}/api"
)

func main() {
	app := fx.New(
		api.Module,
		fx.Invoke(serve),
		fx.NopLogger,
	)

	if app.Err() != nil {
		log.Printf("%+v", app.Err())
	} else {
		app.Run()
	}
}

func serve(lifecycle fx.Lifecycle, router *chi.Mux) {
	//
	// Web server hooks.
	lifecycle.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			port := os.Getenv("{{cookiecutter.project_constant}}_PORT")
			if port == "" {
				port = "{{cookiecutter.project_port}}"
			}

			log.Println("Listening on port " + port)
			go http.ListenAndServe(":"+port, router)

			return nil
		},
	})
}
