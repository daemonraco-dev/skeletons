/** @format */

"use strict";

class {{cookiecutter.project_classname}}Class {
    constructor() {
        this._setListeners();
        this._loadConfirmationModal();
    }
    //
    // Public methods.

    //
    // Private methods.
    _checkBackground() {
        const holder = document.querySelector("#background-overlay");
        const src = document.querySelector("#background-setter");

        if (holder !== null && src !== null) {
            holder.style.backgroundImage = `url(${src.getAttribute(
                "data-src"
            )})`;
            src.remove();
        }
    }
    _clearModal(event) {
        if (event.target.getAttribute("data-{{cookiecutter.project_slug}}-hide-modal") !== null) {
            try {
                bootstrap.Modal.getInstance(
                    event.target.getAttribute("data-{{cookiecutter.project_slug}}-hide-modal")
                ).hide();
            } catch (err) {
                console.error(err);
            }
        }
    }
    _loadConfirmationModal() {
        const modalElem = document.querySelector("#main-confirmation-modal");

        this._confirmationDetails = null;
        this._confirmationModal = new bootstrap.Modal(modalElem, {});
        this._confirmationModalTitle = modalElem.querySelector(
            ".modal-header .title"
        );
        this._confirmationModalMessage = modalElem.querySelector(".modal-body");
        this._confirmationModalYesButton = modalElem.querySelector(
            '[data-toggle="yes"]'
        );

        modalElem.addEventListener("hide.bs.modal", (me) => {
            this._confirmationDetails = null;
        });
        this._confirmationModalYesButton.addEventListener("click", (event) => {
            event.preventDefault();
            if (this._confirmationDetails !== null) {
                this._confirmationDetails.issueRequest(true);
            }
            this._confirmationModal.hide();
        });
    }
    _promptError(event) {
        const modalElem = document.querySelector("#main-error-modal");

        const modalTitle = modalElem.querySelector(".modal-header .title");
        const modalMessage = modalElem.querySelector(".modal-body");

        modalTitle.innerHTML = "Error";

        if (event.detail.xhr && event.detail.xhr.response) {
            modalMessage.innerHTML = event.detail.xhr.response;

            const title = modalMessage.querySelector("title");
            if (title) {
                modalTitle.innerHTML = "Error: " + title.text;
                title.remove();
            }
        } else {
            modalMessage.innerHTML = event.detail.error;
        }

        let modal = bootstrap.Modal.getInstance("#main-error-modal");
        if (!modal) {
            modal = new bootstrap.Modal(modalElem);
        }
        modal.show();
    }
    _setListeners() {
        const mThis = this;
        document.addEventListener("htmx:load", (event) => {
            mThis._clearModal(event);
            mThis._checkBackground();
        });
        document.addEventListener("htmx:responseError", (event) => {
            this._promptError(event);
        });
        document.addEventListener("htmx:confirm", (event) => {
            if (event.detail.question !== null) {
                event.preventDefault();

                this._confirmationDetails = event.detail;
                this._confirmationModalMessage.innerHTML =
                    event.detail.question;
                this._confirmationModal.show();
            }
        });
    }
}

document.addEventListener(
    "DOMContentLoaded",
    () => {
        window.{{cookiecutter.project_classname}} = new {{cookiecutter.project_classname}}Class();
    },
    false
);