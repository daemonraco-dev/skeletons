#!/bin/bash
#
function generals {
    export {{cookiecutter.project_constant}}_PORT={{cookiecutter.project_port}};
    export {{cookiecutter.project_constant}}_ASSETS_DIR="./assets";
    export {{cookiecutter.project_constant}}_CONFIGS_MAIN="./configs/default.yml";
    # export {{cookiecutter.project_constant}}_CONFIGS_LOCAL="./configs/local.yml";
    export {{cookiecutter.project_constant}}_TEMP_DIR="./tmp";
}

generals;
